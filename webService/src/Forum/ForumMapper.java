/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forum;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 *
 * @author Lucas
 */
public interface ForumMapper {
    
    @Insert("INSERT INTO forum(mensagem,usuario) VALUES(#{mensagem}, #{usuario})")
       void insert(Forum forum);
    
    @Update("UPDATE forum SET mensagem = #{mensagem}, usuario= #{usuario} WHERE id= #{id}")
        void update(Forum forum);
        
    @Delete("DELETE from forum WHERE id= #{id}")
        void delete(Forum forum);
        
    @Select("SELECT forum mensagem, usuario WHERE id= #{id}")
        Forum select(int id);
}
