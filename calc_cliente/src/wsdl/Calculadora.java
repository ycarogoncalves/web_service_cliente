package wsdl;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;
public class Calculadora {
    public static void main(String[] args)throws Exception{
        URL url = new URL("http://192.168.43.40:8081/livrodereceitas?wsdl"); 
        QName qname = new QName("http://wsdl/","ReceitaImpServidorService");
        Service ws = Service.create(url,qname);
        Receita r = new Receita();
        
        ReceitaServidor receita = ws.getPort(ReceitaServidor.class);
        
//        r = receita.novo("Teste2", "Testando", "Testing");
        
        r = receita.listarPorId(1);
        
        System.out.println(r.getIngredientes());
        
    }
}
