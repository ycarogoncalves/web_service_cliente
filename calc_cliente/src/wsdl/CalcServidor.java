package wsdl;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface CalcServidor {
    @WebMethod
    public float soma(float a, float b);
    
    @WebMethod
    public float subtracao(float a, float b);
    
    @WebMethod
    public float divisao(float a, float b);
    
    @WebMethod
    public float multiplicacao(float a, float b);
    
    @WebMethod
    public int raiz(int a);
    
    @WebMethod
    public int potencia(int a, int b);
    
    @WebMethod
    public int fatorial(int a);
}
