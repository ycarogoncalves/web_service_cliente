package wsdl;

import javax.jws.WebService;
@WebService(endpointInterface = "src.CalcServidor")
public class CalcImpServidor implements CalcServidor{

    @Override
    public float soma(float a, float b) {
        return a+b;
    }

    @Override
    public float subtracao(float a, float b) {
        return a-b;
    }

    @Override
    public float divisao(float a, float b) {
        if(b==0){
            System.out.println("Impossível ralizar operação!!");
        }else{
             return  a/b;
        }
        return 0;
    }

    @Override
    public float multiplicacao(float a, float b) {
        return a*b;
    }
    
    @Override
    public int raiz(int a){
       return (int) java.lang.Math.sqrt(a);
    }
    @Override
     public int potencia(int a, int b){
        return (int) Math.pow (a, b);      
     }
     public int fatorial(int a){
      if (a<=1) return 1;
        return a* fatorial(a - 1);
     }
}

