/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wsdl;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ReceitaServidor {
    
    @WebMethod
    public Receita novo( String s1, String s2, String s3 );
    
    @WebMethod
    public Receita listarPorId(int id);
    
    
    @WebMethod
    public Receita listarPorNome(String nome);
}
